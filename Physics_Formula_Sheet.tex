\documentclass{article}
\usepackage{amsmath}
\usepackage{charter}
\usepackage{parskip}
\usepackage{dirtytalk}
\usepackage{siunitx}
\usepackage{microtype}
\usepackage[margin=1in]{geometry}
\DeclareSIUnit\atmosphere{atm}
\DeclareSIUnit\torr{Torr}
\DeclareSIUnit\mole{mol}
\DeclareSIUnit\calorie{cal}
\DeclareSIUnit\Calorie{Cal}
\DeclareSIUnit\electronvolt{eV}
%\DeclareSIUnit\volt{V}
\begin{document}

\title{Physics Formulas 2015-2016}
\author{Ryan Zhou \and Jessie Meek \and Jason Zhao }
\maketitle

\clearpage
\vspace*{\stretch{1}}
\begin{center}
\textbf{Special Thanks to:}

My proofreaders\\Jason Zhao, Manuj Shah,\\Luke Sang and Jessie Meek.
\end{center}
\vspace*{\stretch{3}}
\clearpage

\clearpage
\vspace*{\stretch{1}}
\textbf{About this formula sheet}: This formula sheet (is it still a sheet?) has all the formulas we've learned throughout this year, written in \LaTeX\ for readability. There are a lot of formulas, and this sheet aims to collect them all in one place (because who actually wrote them in the back of their notebook?). Even though there is a formula chart provided on the AP test, knowing most of the formulas will be much faster than searching for the right one to use. 

The formulas are organized into sections by chapter, in the order that we learned them. Each section is divided into 3 subsections, containing new units for that chapter, all variables needed for that chapter, and then the formulas in the chapter. The last section, Modern Physics, also has a People section.

If you have any suggestions or find any errors, let Jessie know. 

I hope this is useful to you! 

\textbf{\say{Happy studying!}} - Jessie Meek
\vspace*{\stretch{3}}
\clearpage

\section{Kinematics}
\subsection{Units}
$m$: meters\\
$\si{\second}$: seconds
\subsection{Variables}
$v_{0}$: Initial velocity; $\frac{m}{s}$\\
$v_{f}$: Final velocity; $\frac{m}{\si{\second}}$\\
$a$: Acceleration; $\frac{m}{\si{\second}^2}$\\
$t$: Time; $\si{\second}$\\
$\Delta x$: Displacement; $m$
\subsection{Formulas}
1. First Kinematic:
$\boxed{\vec{v}_f = \vec{v}_0 + \vec{a}t}$

2. Second Kinematic:
$\boxed{\Delta \vec{x} = \vec{v_0t} + \frac{1}{2}\vec{a}t^2}$

3. Third Kinematic:
$\boxed{\vec{v}_{f}^2 = \vec{v}_{0}^2 + 2\vec{a}\Delta \vec{x}}$ 

Rearranging the first kinematic gives us $\vec{a} = \frac{\vec{v}_f - \vec{v}_0}{t}$. Plugging this for $\vec{a}$ in the second kinematic and simplifying gives us...

4. Fourth Kinematic:
$\boxed{\Delta \vec{x} = \frac{t(\vec{v}_f + \vec{v}_0)}{2}}$

Rearranging the first kinematic a different way gives us $\vec{v}_0 = \vec{v}_f - \vec{a}t$. Substituting this for $\vec{v}_0$ in the second kinematic and simplifying again...

5. Fifth Kinematic:
$\boxed{\Delta \vec{x} = \vec{v}_ft - \frac{1}{2}\vec{a}t^2}$

(Don't use 4 and 5, we're not supposed to learn them until Physics C)

\section{Forces}
\subsection{Units}
$N$: Newtons; $\frac{kg\cdot m}{\si{\second}^2}$
\subsection{Variables}
$m$: Mass; $kg$
\begin{itemize}
	\item $m_p$: Mass of planet
\end{itemize}
$a$: Acceleration; $\frac{m}{\si{\second}^2}$
\begin{itemize}
	\item $g$: Acceleration due to gravity
\end{itemize}
$F$: Force; $N$
\begin{itemize}
	\item $F_n$: Normal force to a surface
	\item $W$: Weight
	\item $F_f$: Force due to friction
	\item $F_g$: Force due to gravity between two objects
\end{itemize}
$G$: Universal Gravitational Constant; $6.67 \cdot 10^{-11}\frac{N\cdot m^2}{kg^2}$\\
$\mu$: Coefficient of Friction\\
$r$: Distance between centers of masses; $m$
\begin{itemize}
	\item $r_p$: Radius of planet
\end{itemize}

\subsection{Formulas}
1. Newton's Second Law:
$\boxed{\Sigma \vec{F} = m\vec{a}}$

2. Weight:
$\boxed{\vec{W} = m\vec{g}}$ or in 2D: $\boxed{\vec{W}_x = m\vec{g}\sin\theta}$ and $\boxed{\vec{W}_y = m\vec{g}\cos\theta}$

3. Friction:
$\boxed{\vec{F}_f = \vec{F}_n\mu}$

4. Gravitational Force between 2 masses:
$\boxed{\vec{F}_g = G\frac{m_1m_2}{r^2}}$

5. Acceleration on a planet due to gravity:
$\boxed{\vec{g} = G\frac{m_p}{r_p^2}}$

\section{Uniform Circular Motion}
\subsection{Variables}
$\vec{v}$: Velocity; $\frac{m}{\si{\second}}$\\
$r$: Radius; $m$\\
$\vec{F_c}$: Centripetal force; $N$\\ 
$\vec{a}_c$: Centripetal acceleration; $\frac{m}{\si{\second}^2}$

\subsection{Formulas}
1. Centripetal Force:
$\boxed{\vec{F_c} = m\vec{a}_c}$

2. Centripetal Acceleration:
$\boxed{\vec{a}_c = \frac{\vec{v}^2}{r}}$

\section{Work, Energy, Power and Momentum}
\subsection{Units}
$J$: Joules; $N \cdot m$\\
$W$: Watts; $\frac{J}{\si{\second}}$
\subsection{Variables}
$m$: Mass; $kg$\\
$v$: Velocity; $\frac{m}{\si{\second}}$\\
$W$: Work; $\si{\joule}$\\
$k$: Spring Constant; $\frac{N}{m}$\\
$x$: Amplitude of spring; $m$\\
$A$: Maximum displacement from equilibrium; $m$\\
$P$: Power; $W$\\
$\vec{p}$: Momentum; $N\cdot \si{\second} = \frac{kg\cdot m}{\si{\second}}$\\
$y$: Height; $\si{\meter}$\\
$l$: Length of ``vine''; $\si{\meter}$\\
$\theta$: Angle of vine with normal\\
$E$ or $U$: Energy; $J$
\begin{itemize}
	\item $K$: Kinetic Energy
	\item $U_g$: Gravitational Potential Energy
	\item $U_s$: Spring Potential Energy
	\item $Q$: Heat from friction
\end{itemize}
\subsection{Formulas}
1. Work:
$\boxed{W = \vec{F}\cdot d = \vec{F}\cdot d\cos\theta}$

2. Kinetic Energy:
$\boxed{K = \frac{1}{2}m\vec{v}^2}$

3. Gravitational Potential:
$\boxed{U_g = mgh}$

4. Work-Energy Theorem:
$\boxed{\Sigma W = \Delta K = \frac{1}{2}m\vec{v}_f^2-\frac{1}{2}m\vec{v}_0^2}$

5. Hooke's Law (Spring Force):
$\boxed{\vec{F}_s = -kx}$

6. Spring Potential Energy:
$\boxed{U_s = \frac{1}{2}kx^2}$ or $\boxed{U_s = \frac{1}{2}kA^2}$ at max displacement\\
\textbf{``Remember the freaking A for vertical springs!''} - Jason Zhao

7. Heat from friction:
$\boxed{Q_{level} = mg\mu d}$ or $\boxed{Q_{incline} = mg\cos\theta\mu d}$

8. Power:
$\boxed{P = \frac{W}{t} = \frac{E}{t} = F\cdot v = F\cdot v\cos\theta}$

9. Momentum:
$\boxed{\vec{p} = m\vec{v}}$

10. Impulse ("Jimpulse", change in momentum):
$\boxed{\vec{J} = \Delta\vec{p} = m\vec{v}_f - m\vec{v}_0 = \vec{F}\cdot t}$

11. Center of mass:
$\boxed{\frac{m_1x_1 + m_2x_2 + \cdots + m_nx_n}{m_1 + m_2 + \cdots + m_n}}$

\textbf{Note}: In the Center of mass formula, $m_n$ represent mass of the $n^{th}$ object ($kg$), and $x_n$ represents the distance of the object from a reference point ($m$). The variable $x$ in this case has nothing to do with springs. Sorry for the confusion.

12. ``Vine'' Equation:
$\boxed{\Delta y = l(1-\cos\theta)}$

\section{Angular}
\subsection{Units}
radians: Angle measure; $2\pi$ radians = $360\si{\degree} = 1$ revolution
\subsection{Variables}
$\Delta \vec{\theta}$: Angular displacement; radians or $\si{\degree}$\\
$\vec{\omega}$: Angular velocity; $\frac{radians}{\si{\second}}$\\
$\vec{\alpha}$: Angular acceleration; $\frac{radians}{\si{\second}^2}$\\
$\vec{v}$: Linear velocity; $\frac{m}{\si{\second}}$;\\
$\vec{a}$: Tangential acceleration; $\frac{m}{\si{\second}^2}$\\
$\vec{a}_c$: Centripetal acceleration; $\frac{m}{\si{\second}^2}$\\
$r$: Radius; $m$\\
$s$: Arc length; $m$\\
$\tau$: Torque; $N\cdot m$\\
$I$: Rotational Inertia or Moment of Inertia; $kg\cdot m^2$\\
$W$: Work; $J$\\
$P$: Power; $W$\\
$\vec{p}$: Linear momentum; $N\cdot \si{\second} = \frac{kg\cdot m}{\si{\second}}$\\
$\vec{L}$: Angular momentum; $\frac{kg\cdot m^2}{\si{\second}}$
\subsection{Formulas}
1. 1st Angular Kinematic:
$\boxed{\vec{\omega}_f = \vec{\omega}_0t + \vec{\alpha}t}$

2. 2nd Angular Kinematic:
$\boxed{\Delta \vec{\theta} = \vec{\omega}_0t + \frac{1}{2}\vec{\alpha}t^2}$

3. 3rd Angular Kinematic:
$\boxed{\vec{\omega}_f^2 = \vec{\omega}_0^2 + 2\vec{\alpha}\Delta\vec{\theta}}$

4. Arc Length:
$\boxed{s = r\theta}$

5. Linear Velocity:
$\boxed{\vec{v} = r\vec{\omega}}$

6. Tangential Acceleration: 
$\boxed{\vec{a} = r\vec{\alpha}}$

7. Centripetal Acceleration:
$\boxed{\vec{a}_c = r\vec{\omega}^2}$

8. Torque:
$\boxed{\tau = r\times\vec{F} = r\times\vec{F}\sin\theta}$

9. Rotational Inertia or Moment of Inertia for a point mass:
$\boxed{I = mr^2}$

\textbf{IMPORTANT}: This formula only applies to a \textbf{POINT MASS}. Do \textbf{NOT} use this to find the rotational inertia of anything else.

10. Net Torque:
$\boxed{\Sigma\tau = I\vec{\alpha}}$

11. Work:
$\boxed{W = \tau\cdot\theta}$

12. Power:
$\boxed{P = \tau\cdot\omega}$

13. Angular Momentum:
$\boxed{\vec{L} = I\omega = r\times\vec{p}}$

\section{Simple Harmonic Motion}
\subsection{Units}
$\si{\hertz}$: Hertz; $\frac{1}{\si{\second}}$
\subsection{Variables}
$T$: Period, time for one cycle; $\frac{\si{\second}}{cycle} = \si{\second}$\\
$f$: Frequency, numbers of cycles per second; $\frac{1}{\si{\second}} = \si{\second}^{-1} = \si{\hertz}$\\
$m$: Mass of object; $kg$\\
$k$: Spring constant;  $\frac{N}{m}$\\
$l$: Length of pendulum; $m$\\
$g$: Acceleration due to gravity; $\frac{m}{\si{\second}^2}$\\
$v$: Velocity; $\frac{m}{\si{\second}}$
\begin{itemize}
	\item $v_{sound}$: Speed of sound; $343 \frac{m}{\si{\second}}$ at $20\si{\degree}C$
\end{itemize}
$T_C$: Temperature; $\si{\degree}C$\\
$\lambda$: Wavelength; $m$\\
$L$: Length of string; $m$
\subsection{Formulas}
1. Frequency:
$\boxed{f = \frac{1}{T}}$

2. Period of a Spring:
$\boxed{T = 2\pi\sqrt{\frac{m}{k}}}$

3. Period of a Pendulum:
$\boxed{T = 2\pi\sqrt{\frac{l}{g}}}$

4. Speed of Sound in Air:
$\boxed{v_{sound} = 331 + 0.6T_C}$

5. Wave Equation:
$\boxed{v = f\lambda}$

\textbf{IMPORTANT}: Pendulums demonstrate simple harmonic motion if the drop angle $\theta \leq 15\si{\degree}$. Pay attention!!!

6. Wavelength in a Closed Tube:
$\boxed{L = \frac{n}{4}\lambda}$ where $n = 1, 3, 5 \cdots$

7. Wavelength in an Open Tube:
$\boxed{L = \frac{n}{2}\lambda}$ where $n = 1, 2, 3 \cdots$

8. Wavelength on a String:
$\boxed{L = \frac{n}{2}\lambda}$ where $n = 1, 2, 3 \cdots$

9. Velocity of a wave on a String:
$\boxed{v = \sqrt{\frac{T}{\frac{m}{L}}}}$

10. Beat Frequency:
$\boxed{f_{beat} = |f_1 - f_2|}$

\section{Fluids}

\subsection{Units}
$\si{\atmosphere}$: Atmospheres, main unit of pressure\\
$\si{\pascal}$: Pascals; $\frac{\si{\newton}}{m^2}$; $101325\si{\pascal} = 1\si{\atmosphere}$\\
$\si{\torr}=\si{\mmHg}$: Alternate unit of pressure; $760\si{\torr} = 760\si{\mmHg} = 1\si{\atmosphere}$
\subsection{Variables}
$v$: Velocity; $\frac{m}{\si{\second}}$\\
$P$: Pressure; $\si{\atmosphere}, \si{\pascal}, \si{\torr}, \si{\mmHg}$\\
$\rho$: Density; $\frac{\si{\kilogram}}{\si{\meter}^3}$\\
$I$: Volumetric flow rate; $\frac{m^3}{\si{\second}}$\\
$V$: Volume; $m^3$\\
$m$: Mass; $\si{\kilogram}$\\
$A$: Area of surface; $m^2$\\
$g$: Acceleration due to gravity; $\frac{m}{\si{\second}^2}$\\
$h$: Depth of fluid; $m$\\
$\vec{F}$: Force; $\si{\newton}$
\begin{itemize}  
    \item $\vec{F}_n$: Normal force, also the apparent weight
    \item $\vec{F}_B$: Buoyant force
\end{itemize}
\subsection{Formulas}
1. Density:
$\boxed{\rho = \frac{m}{V}}$

2. Pressure:
$\boxed{P = \frac{\vec{F}}{A}}$

3. Gauge Pressure (Pressure at a depth due to liquid): 
$\boxed{P = \rho gh}$

4. Total Pressure at a depth:
$\boxed{P_{total} = P_{atmosphere} + P_{gauge}}$

5. Pascal's Principle:
$\boxed{\frac{F_1}{A_1} = \frac{F_2}{A_2}}$

6. Buoyant Force:
$\boxed{\vec{F}_B = PA = \rho gV_{displaced}}$

7. Apparent Weight:
$\boxed{\vec{F}_n = W - F_B}$

6. Volumetric Flow Rate:
$\boxed{I = Av}$

8. Continuity Equation:
$\boxed{I = A_1v_1 = A_2v_2}$

9. Bernoulli's Principle:
$\boxed{P_1 + \frac{1}{2}\rho v_1^2 + \rho gh_1 = P_2 + \frac{1}{2}\rho v_2^2 + \rho gh_2}$

\section{Thermodynamics}
\subsection{Units}
$\si{\calorie}$: calorie; $1\si{\calorie} = 4.186\si{\joule}$\\
$\si{\Calorie}$: Calorie; $1\si{\Calorie} = 4186\si{\joule}$
\subsection{Variables}
$Q$: Heat; $\si{\joule}$\\
$m$: Mass; $\si{\kilogram}$\\
$P$: Pressure; $\si{\atmosphere}, \si{\mmHg}, \si{\torr}, \si{\pascal}$\\
$V$: Volume; $m^3$\\
$K$: Kinetic energy; $\si{\joule}$\\
$R$: Gas constant; $8.315\frac{\si{\joule}}{\si{\mole}\cdot\si{\kelvin}}$\\
$k$: Boltzmann constant; $1.38 \cdot 10^{-23} \frac{\si{\joule}}{\si{\kelvin}}$\\
$U$: Internal energy; $\si{\joule}$\\
$W$: Work; $\si{\joule}$\\
$T$: Temperature; $\si{\kelvin}, \si{\degree}C$\\
$n$: Number of moles\\
$N$: Number of molecules\\
$c$: Specific heat; $\frac{\si{\joule}}{\si{\kilogram}\si{\degree}C}$
\begin{itemize}   
    \item $c_p$: molar heat capacity at constant pressure; $\frac{\si{\joule}}{\si{\mole} \si{\degree}C}$
    \item $c_v$: molar heat capacity at constant volume; $\frac{\si{\joule}}{\si{\mole} \si{\degree}C}$
\end{itemize}
\subsection{Formulas}
1. Heat:
$\boxed{Q = mc\Delta T}$

2. Boyle's Law:
$\boxed{P_1V_1 = P_2V_2}$

3. Charles' Law:
$\boxed{\frac{V_1}{T_1} = \frac{V_2}{T_2}}$

4. Gay-Lussac:
$\boxed{\frac{P_1}{T_1} = \frac{P_2}{T_2}}$

5. Combined Gas Law:
$\boxed{\frac{P_1V_1}{T_1} = \frac{P_2V_2}{T_2}}$

6. Ideal Gas Equation:
$\boxed{PV = nRT = NkT}$

7. Average Translational Kinetic Energy:
$\boxed{K = \frac{3}{2}kT}$

8. Internal Energy:
$\boxed{U = \frac{3}{2}NkT = \frac{3}{2}nRT}$

9. Root-Mean-Squared Velocity:
$\boxed{v_{rms} = \sqrt{\frac{3kT}{m}}}$

10. 1st Law of Thermodynamics:
$\boxed{\Delta U = \Delta Q + \Delta W}$

11. Work:
$\boxed{W = -P\Delta V}$

12. Isobaric Process (Constant Pressure):
$\boxed{\Delta Q = \frac{5}{2}nR\Delta T = nc_p\Delta T}$

13. Isometric/Isochoric/Isovolumetric Process (Constant Volume): 
$\boxed{\Delta W = 0}$ and $\boxed{\Delta Q = \frac{3}{2}nR\Delta T = nc_v\Delta T}$

14. Adiabatic Process (No Heat Added/Removed):
$\boxed{\Delta Q = 0}$

15. Isothermal Process (Constant Temperature): 
$\boxed{\Delta T = 0}$ so $\boxed{\Delta U = 0}$ and $\boxed{\Delta W = \Delta Q = P\Delta V}$

\section{Electrostatics}
\subsection{Units}
$\si{\second}$: seconds\\
$\si{\coulomb}$: Coulombs\\
$\si{\ampere}$: Ampere; $\frac{\si{\coulomb}}{\si{\second}}$\\
$\si{\farad}$: Farads\\
$\si{\volt}$: Volts; $\frac{\si{\joule}}{\si{\coulomb}}$\\
$\si{\electronvolt}$: Electronvolt; $1\si{\electronvolt} = 1.6 \cdot 10^{-19}\si{\joule}$
\subsection{Variables}
$\epsilon_0$: Permittivity of free space; $8.854\cdot 10^{-12} \frac{C^2}{N\cdot m^2}$\\
$k$: Coulomb's constant; $\frac{1}{4\pi \epsilon_0} = 9 \cdot 10^9 \frac{N\cdot m^2}{C^2}$\\
$k$: Dielectric constant\\
\textbf{Note}: The symbol for Coulomb's constant and the Dielectric constant are the same. I am going to use $\frac{1}{4\pi \epsilon_0}$ for Coulomb's constant.

$q, Q$: Electric charge; $C$\\
$r$: Distance between centers of objects; $\si{\meter}$\\
$V$: Voltage or Potential; $\si{\volt}$\\
$W$: Work; $\si{\joule}$\\
$C$: Capacitance\\
$A$: Area of plates (capacitor); $\si{\meter}^2$\\
$d$: Distance between plates (capacitor); $\si{\meter}$
\subsection{Formulas}
1. Coulomb's Law (Electrostatic Force): 
$\boxed{\vec{F}_e = \frac{1}{4\pi \epsilon_0}\cdot\frac{|Q_1q_2|}{r^2}}$

2. Electric Field: 
$\boxed{\vec{E} = \frac{1}{4\pi \epsilon_0}\cdot\frac{|q|}{r^2}}$

3.Electrostatic Force:
$\boxed{\vec{F}_e = q\vec{E}}$

4. Voltage (Uniform E-Field):
$\boxed{V = \vec{E}d}$

5. Work:
$\boxed{W = qV = q\vec{E}d}$

6. Voltage (Non-uniform E-Field):
$\boxed{V = \frac{1}{4\pi \epsilon_0}\cdot\frac{|q|}{r}}$

7. Capacitance:
$\boxed{C = \frac{k\epsilon_0A}{d}}$

8. Charge of a Capacitor:
$\boxed{Q = CV}$

9. Energy in a Capacitor:
$\boxed{U_{cap} = \frac{1}{2}QV = \frac{1}{2}CV^2 = \frac{1}{2}\cdot\frac{Q^2}{C}}$

\section{Circuits}
\textbf{Note}: Formulas will only get you so far in circuits! Be sure you know how to ``solve" circuits too!
\subsection{Units}
$\si{\ohm}$: Ohm (Resistance)
\subsection{Variables}
$\rho$: Density; $\frac{\si{\kilogram}}{\si{\meter}^3}$\\
$L$: Length of wire; $\si{\meter}$\\
$A$: Area of cross-section of wire; $\si{\meter}^2$\\
$R$: Resistance; $\si{\ohm}$\\
$V$: Voltage; $\si{\volt}$\\
$I$: Current; $\si{\ampere}$\\
$Q$: Charge; $\si{\coulomb}$\\
$t$: Time; $\si{\second}$\\
$P$: Power; $\si{\watt}$\\
$E$: Energy; $\si{\joule}$\\
$\epsilon$: Electromotive force (EMF); $\si{\volt}$\\
$C$: Capacitance; $\si{\farad}$
\subsection{Formulas}
1. Resistance of a Wire:
$\boxed{R_{wire} = \rho\frac{L}{A}}$

2. Ohm's Law:
$\boxed{V = IR}$

3. Charge:
$\boxed{Q = It}$

4. Resistors in Series:
\begin{itemize}
    \item Resistance: $\boxed{R_{total} = R_1 + R_2 + R_3 + \cdots}$
    \item Current: Current equal at each resistor
    \item Voltage: $\boxed{V_{total} = V_1 + V_2 + V_3 + \cdots}$
\end{itemize}

5. Kirchhoff's Voltage Law (Closed Loop): 
$\boxed{\Sigma V = 0}$ (Voltage between two points is path-independent)

6. Resistors in Parallel:
\begin{itemize}
    \item Resistance: $\boxed{R_{total} = \frac{1}{\frac{1}{R_1} + \frac{1}{R_2} + \cdots}}$
    \item Current: $\boxed{I_{total} = I_1 + I_2 + \cdots}$
    \item Voltage: Voltage equal at each resistor
\end{itemize}

7. Power:
$\boxed{P = VI = I^2R = \frac{V^2}{R}}$

8. Energy:
$\boxed{E = Pt = VIt = I^2Rt = \frac{V^2t}{R}}$

9. Real Battery Equation:
$\boxed{V_{battery} = \epsilon - V_{internal} = \epsilon - IR_{internal} = IR_{external}}$

10. Capacitors in Series:
\begin{itemize}
    \item Capacitance: $\boxed{C_{total} = \frac{1}{\frac{1}{C_1} + \frac{1}{C_2} + \cdots}}$
    \item Charge: Charge equal at each capacitor
    \item Voltage: $\boxed{V_{total} = V_1 + V_2 + V_3 + \cdots}$
\end{itemize}

11. Capacitors in Parallel:
\begin{itemize}
    \item Capacitance: $\boxed{C_{total} = C_1 + C_2 + C_3 + \cdots}$
    \item Charge: $\boxed{Q_{total} = Q_1 + Q_2 + \cdots}$
    \item Voltage: Voltage equal at each capacitor
\end{itemize}

\section{Magnetism}
\textbf{RH/LH Rules}: RH for positive charge, LH for negative. Thumb points in direction of force, index in direction of velocity and other 3 fingers in direction of B-Field.\\
\textbf{RH Rule for Current}: Pretend to grasp wire in palm with thumb pointing in direction of current. Index fingers mean outward B-field and palm means inwards B-field.
\subsection{Units}
$\si{\tesla}$: Tesla (Magnetic Field Strength)\\
$\si{\weber}$: Weber; $\si{\tesla}\si{\meter}^2$
\subsection{Variables}
$\vec{B}$: Magnetic field; $\si{\tesla}$\\
$q$: Charge; $\si{\coulomb}$\\
$\vec{v}$: Velocity; $\frac{\si{\meter}}{\si{\second}}$\\
$I$: Current; $\si{\ampere}$\\
$l$: Length; of wire $\si{\meter}$\\
$\mu_0$: Permeability of free space; $4\pi \cdot 10^{-7} \frac{\si{\tesla}\si{\meter}}{\si{\ampere}}$\\
$r$: Distance from wire; $\si{\meter}$\\
$\epsilon$: Electromotive force; $\si{\volt}$\\
$N$: Number of coils\\
$\Phi$: Magnetic flux; $\si{\weber}$\\
$t$: Time; $\si{\second}$\\
$A$: Area of loop; $\si{\meter}^2$
$\vec{F}$: Force; $\si{\newton}$
\begin{itemize}
   \item $\vec{F}_b$: Magnetic force    
\end{itemize}
\subsection{Formulas}
1. Magnetic Force:
$\boxed{\vec{F}_b = q\vec{v}\times\vec{B}}$

2. Magnetic Force on a Current:
$\boxed{\vec{F}_b = Il\times B}$

3. Magnetic Field Around a Wire:
$\boxed{\vec{B} = \frac{\mu_0}{2\pi}\cdot\frac{I}{r}}$

4. Magnetic Force Between Two Wires:
$\boxed{\vec{F}_b = I_1l\frac{\mu_0}{2\pi}\cdot\frac{I_2}{r}}$\\
\textbf{Remember}: ``We come together when we walk the same path..."

5. Faraday's Law of Electromagnetic Induction:
$\boxed{\epsilon = N\frac{\Delta\Phi}{\Delta t} = \frac{N\vec{B}A\cos\theta}{t}}$

6. Magnetic Flux:
$\boxed{\Phi = \vec{B}A\cos\theta}$

\textbf{Lenz's Law}: Induced magnetic field from the induced current opposes the magnetic field that created the induced current. ``Nature abhors change..."

7. Electromotive Force:
$\boxed{\epsilon = \vec{B}l\vec{v}}$

8. Transformers: 
$\boxed{\frac{V_2}{V_1} = \frac{N_2}{N_1}}$ and $\boxed{V_1I_{in} = V_2I_{out}}$

\section{Light}
\subsection{Variables}
$v$: Velocity; $\frac{\si{\meter}}{\si{\second}}$\\
$c$: Speed of light in vacuum; $3\cdot 10^8 \frac{\si{\meter}}{\si{\second}}$\\
$n$: Index of refraction; Ranges from $1$ to $2.5$\\
$t$: Thickness; $\si{\meter}$\\
$d$: Distance between slits (2 PC) \textbf{OR} width of single slit (1 PC) \textbf{OR} distance from object/image to lens; $\si{\meter}$\\
$f$: Focal length; $\si{\meter}$\\
$m$: Magnification\\
$h$: Height of object; $\si{\meter}$\\
$l$: Distance to screen; $\si{\meter}$\\
$x$: Distance between bright spots (2 PC) \textbf{OR} Distance between central max and first dark spot (1 PC); $\si{\meter}$\\
$\lambda$: Wavelength; $\si{\nano}\si{\meter}$
\begin{itemize}
	\item Red Light: $750 \si{\nano}\si{\meter}$
	\item Violet Light: $400 \si{\nano}\si{\meter}$
\end{itemize}
$f$: Frequency; $\si{\hertz}$
\begin{itemize}
	\item Red Light: $4\cdot10^{14}\si{\hertz}$
	\item Violet Light: $7.5\cdot10^{14}\si{\hertz}$
\end{itemize}
$\theta$: Angle; measured to \textbf{NORMAL}
\begin{itemize}
	\item $\theta_i$: Angle of incidence
	\item $\theta_r$: Angle of refraction/reflection
	\item $\theta_c$: Critical angle
\end{itemize}
\subsection{Formulas}
1. Wave Equation:
$\boxed{v = f\lambda}$

2. Index of Refraction:
$\boxed{n = \frac{c}{v}}$

3. Law of Reflection:
$\boxed{\theta_i = \theta_r}$

4. Snell's Law (Refraction):
$\boxed{n_1\sin\theta_i = n_2\sin\theta_r}$\\
\textbf{Law of Refraction}: If the wave speeds up in new material, it bends away from normal, and if it slows down it bends torwards normal.

5. Critical Angle: 
$\boxed{\sin\theta_c = \frac{n_2}{n_1}}$ where $n_2 < n_1$

6. Waves in Prisms:
$\boxed{n_1\lambda_1 = n_2\lambda_2}$\\
\textbf{Important}: Frequency does NOT change!
\clearpage
Table of m values:

\begin{tabular}{lll}
             & 1 PC & 2 PC \\
Constructive & $m+\frac{1}{2}$ & $m$ \\
Destructive  & $m$ & $m+\frac{1}{2}$ 
\end{tabular}

\textbf{Note}: I'll use $\rule{2mm}{0.15mm}$ in the following formulas to represent $m$. In actual problems, look at number of phase changes and type of interference to get the correct $m$ expression.

7. Thin Film Interference:
$\boxed{2nt = \rule{2mm}{0.15mm}\lambda}$

8. Double Slit (2 PC) and Single Slit (1 PC) interference: 
$\boxed{d\sin\theta = \rule{2mm}{0.15mm}\lambda}$ and $\boxed{\tan\theta = \frac{x}{l}}$

9. Distance from Central Max to 1st Order Max in Single Slit:
$\boxed{\frac{3}{2}\lambda}$

10. Thin Lens Equation:
$\boxed{\frac{1}{d_i} + \frac{1}{d_o} = \frac{1}{f}}$

11. Magnification:
$\boxed{m = -\frac{d_i}{d_o} = \frac{h_i}{h_o}}$

\section{Modern}
\subsection{Variables}
$E$: Energy; $\si{\joule}$ or $\si{\electronvolt}$\\
$n$: Quantum number\\
$h$: Planck's Constant; $6.626\cdot 10^{-31} \frac{\si{\joule}}{\si{\second}} = 4.14\cdot 10^{-15} \frac{\si{\electronvolt}}{\si{\second}}$\\
$f$: Frequency; $\si{\hertz}$\\
$R$: Rigberg's Constant; $1.097\cdot 10^7 m^{-1}$\\
$k$: Coulomb's constant; $\frac{1}{4\pi \epsilon_0} = 9 \cdot 10^9 \frac{N\cdot m^2}{C^2}$\\
$e$: Charge of electron; $1.67\cdot 10^{-19}\si{\coulomb}$\\
$m$: Mass; $\si{\kilogram}$\\
$\Phi$: Work function, Energy required to remove an electron; $\si{\joule}$ or $\si{\electronvolt}$
$V_s$: Stopping voltage; $\si{\volt}$\\
$K$: Kinetic Energy; $\si{\joule}$\\
$c$: Speed of light; $3\cdot 10^8 \frac{\si{\meter}}{\si{\second}}$\\
$p$: Momentum; $\si{\newton}\cdot\si{\second}$

\subsection{Particles}
$n$: Neutron\\
$p$: Proton\\
$e^-$: Electron\\
$e^+$: Positron (positive electron)\\
$v$: Neutrino\\
$\bar{v}$: Antineutrino\\
$\alpha$: Alpha particle; $^4_2$He\\
$\beta$: Beta Particle
\begin{itemize}
	\item Negative: $n \rightarrow p +$ $^0_-\beta$
	\item Positive: $p \rightarrow n +$ $_+^0\beta$
\end{itemize}
$\gamma$: Gamma ray, assosciated with alpha decay

\subsection{Formulas}
1. Mass-Energy Equivalence:
$\boxed{E = mc^2}$

2. Planck's Law:
$\boxed{E = nhf = \frac{nhc}{\lambda}}$

3. Balmer Series (Visible):
$\boxed{\frac{1}{\lambda} = R(\frac{1}{2^2} - \frac{1}{n^2})}$ for $n = 3, 4, 5, 6$

4. Lyman Series (UV):
$\boxed{\frac{1}{\lambda} = R(\frac{1}{1^2} - \frac{1}{n^2})}$ for $n = 2, 3, 4, 5, 6$

5. Paschen Series (IR):
$\boxed{\frac{1}{\lambda} = R(\frac{1}{3^2} - \frac{1}{n^2})}$ for $n = 4, 5, 6$

6. Bohr Radius (Hydrogen only): 
$\boxed{r = \frac{n^2h^2}{4\pi^2ke^2m}}$

7. Bohr Energy (Hydrogen only):
$\boxed{E = -\frac{2\pi^2k^2e^4m}{n^2h^2}}$

8. Energy of a Photon:
$\boxed{E = hf = \frac{hc}{\lambda}}$

9. Momentum of a photon:
$\boxed{\vec{p}_{photon} = \frac{E}{c} = \frac{hf}{c} = \frac{h}{\lambda}}$

10. Photoeletric Effect:
$\boxed{K_{max} = hf - \Phi}$ ($\si{\joule}$) or $\boxed{V_s = hf - \Phi}$ ($\si{\electronvolt}$)

11. Work Function at Cutoff Frequency:
$\boxed{\Phi = hf}$

12. Pair production/Annihilation:
$\boxed{E_{photon} = 2mc^2}$

13. X-Ray Production:
$\boxed{qV = \frac{1}{2}mv^2 = \frac{hc}{\lambda}}$

14. COE Compton Scattering:
$\boxed{hf_i = \frac{1}{2}m_ev^2 + hf_f}$

15. COp Compton Scattering:
$\boxed{\frac{E}{c} = mv + \frac{E}{c}}$

16. De Broigle:
$\boxed{\lambda = \frac{h}{mv}}$

17. Mass Defect:
$\boxed{m_{total} = m_{1} + m_{2} + m_{energy}}$

\subsection{People}
\textbf{JJ Thompson}: Created the Plum Pudding Model and found charge/mass ratio.

\textbf{Rovert Millikan}: Conducted Oil drop experiment and determined the mass and charge of an electron.

\textbf{Ernest Rutherford}: Bombarded Gold foil with $\alpha$-particles. Showed that the plum pludding model is incorrect and gave evidence for the planetary model.

\textbf{Max Planck}: Came up with quantum theory suggesting energy was quantized.

\textbf{Balmer, Lyman and Paschen}: Created series (that are extremely similar) modeling the emission spectra of Hydrogen in visible, UV and IR respectively.

\textbf{Niels Bohr}: Quantized angular momentum, created Bohr model of atom, found equations for Energy and Radius of atoms according to his model.

\textbf{Albert Einstein}: Demonstrated the photoelectric effect which proved that light could act as a particle. 

\textbf{Louis DeBroglie}: Showed that all particles demonstrated wave properties.
\end{document}